@extends('layouts.app')

@section('content')
<style>
  .uper {
    margin-top: 40px;
  }
</style>
<div class="uper">
  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}
    </div><br />
  @endif
      <div class="card-header">
          Add Section
      </div>
  <table class="table table-striped">
    <tbody>
        @if ($sections)
            @foreach($sections as $section)
                    <tr>
                        <td><img src="{{asset('storage/app/logo/'.$section->id.'/'.$section->logo)}}" alt=""></td>
                        <td>
                            <strong>{{$section->name}}</strong>
                            <p>{{$section->description}}</p>
                        </td>
                        <td>
                            <strong>Users</strong>
                            @foreach($section->users as $user)
                                <p>{{$user->name}}</p>
                            @endforeach
                        </td>
                        <td><a href="{{ route('sections.edit', $section->id)}}" class="btn btn-primary">Edit</a></td>
                        <td>
                            <form action="{{ route('sections.destroy', $section->id)}}" method="post">
                              @csrf
                                    @method('DELETE')
                                    <button class="btn btn-danger" type="submit">Delete</button>
                            </form>
                        </td>
                    </tr>
            @endforeach
        @endif
    </tbody>
  </table>
<div>
{{ $sections->links() }}
@endsection
