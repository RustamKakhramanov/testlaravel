@extends('layouts.app')

@section('content')
    <style>
        .uper {
            margin-top: 40px;
        }
    </style>
    <div class="card uper">
        <div class="card-header">
            Edit Share
        </div>
        <div class="card-body">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div><br />
            @endif
            <form enctype="multipart/form-data"  method="post" action="{{ route('sections.update', $section->id) }}">
                @method('PATCH')
                @csrf
                <div class="form-group">
                    <label for="name">Name:</label>
                    <input type="text" class="form-control" name="name" value="{{$section->name}}" required/>
                </div>
                <div class="form-group">
                    <label for="description">Description :</label>
                    <textarea  class="form-control" name="description" value="{{$section->description}}" placeholder="Enter Descrtiption" rows="9" required>
                    </textarea>
                </div>
                <div class="custom-file">
                    <input type="file" class="custom-file-input" id="validatedCustomFile" name="logo" accept=".png, .jpg, .jpeg, .svg" required>
                    <label class="custom-file-label" for="validatedCustomFile">Choose file...</label>
                    <div class="invalid-feedback">Example invalid custom file feedback</div>
                </div>
                @if ($section->users)
                    <div>
                        <h2 class="mt-2 mb-2">Users</h2>
                        @foreach($section->users as $user)
                            <div class="form-check">
                                <input type="checkbox" class="form-check-input" checked name="users[{{$user->id}}]"  id="user-{{$user->id}}">
                                <label class="form-check-label" for="user-{{$user->id}}">{{$user->name}} ( <span class="text-primary">{{$user->email}}</span> )</label>
                            </div>
                        @endforeach
                        @foreach($users as $user)
                            <div class="form-check">
                                <input type="checkbox" class="form-check-input" name="users[{{$user['id']}}]"  id="user-{{$user['id']}}">
                                <label class="form-check-label" for="user-{{$user['id']}}">{{$user['name']}} ( <span class="text-primary">{{$user['email']}}</span> )</label>
                            </div>
                        @endforeach
                    </div>
                @endif
                <button type="submit" class="btn btn-primary">Update</button>
            </form>
                <a href="{{ route('sections.index')}}" class="btn btn-success">Back</a>
        </div>
    </div>
@endsection
