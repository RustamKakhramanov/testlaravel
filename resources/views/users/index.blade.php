@extends('layouts.app')

@section('content')
<style>
  .uper {
    margin-top: 40px;
  }
</style>
<div class="uper">
  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}
    </div><br />
  @endif
    <div>
        <a href="{{ route('users.create')}}" class="btn btn-success">Create User</a>
    </div>
  <table class="table table-striped">
    <thead>
        <tr>
          <td>ID</td>
          <td>Name</td>
          <td>Email</td>
          <td>Registration date</td>
          <td colspan="2">Action</td>
        </tr>
    </thead>
    <tbody>
        @if ($users)
            @foreach($users as $user)
                    <tr>
                        <td>{{$user->id}}</td>
                        <td>{{$user->name}}</td>
                        <td>{{$user->email}}</td>
                        <td>{{$user->created_at}}</td>
                        <td><a href="{{ route('users.edit', $user->id)}}" class="btn btn-primary">Edit</a></td>
                        <td>
                            <form action="{{ route('users.destroy', $user->id)}}" method="post">
                              @csrf
                                @if (\Illuminate\Support\Facades\Auth::id() !== $user->id)
                                    @method('DELETE')
                                    <button class="btn btn-danger" type="submit">Delete</button>
                                @endif
                            </form>
                        </td>
                    </tr>
            @endforeach
        @endif
    </tbody>
  </table>
<div>
{{ $users->links() }}
@endsection
