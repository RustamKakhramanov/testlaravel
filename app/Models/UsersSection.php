<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UsersSection extends Model
{
    protected $table = 'users_sections';
    protected $fillable = [
        'user_id',
        'section_id',
    ];
    public $timestamps = false;
}
