<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
/**
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property string $logo
 * @property  $users
 */
class Section extends Model
{
   protected $table  = 'sections',
            $fillable = [
                'name',
                'description',
                'logo',
   ];

    /**
     * Пользователи, принадлежащие к секциям.
     */
    public function users()
    {
        return $this->belongsToMany('App\User', 'users_sections');
    }

    public function usersSections()
    {
        return $this->hasMany('App\Models\UsersSection');
    }
}
