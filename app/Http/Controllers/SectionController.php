<?php

namespace App\Http\Controllers;

use App\Models\Section;
use \App\User;
use Faker\Provider\File;
use Illuminate\Http\Request;

class SectionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sections = Section::paginate(10);
        return view('sections.index', compact('sections'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::select('id','name', 'email')->get();
        return view('sections.create', compact('users'));
    }
    /**
     * Store a newly created resource in storage.
     *Запись секции
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'description' => ['required', 'string'],
            'logo' => 'required|image|mimes:jpeg,png,jpg,svg|max:2048',
            'users' => 'required',
        ]);
        //Валидация, определение путей
        $imageName = $request->file('logo')->getClientOriginalName();
        //Сохраняем изображение, затем записываем в базу данные
        $section = Section::create([
            'name' => $request->get('name'),
            'description'=> $request->get('description'),
            'logo'=> $imageName,
        ]);
        //ЗАпись изображения
        $request->file('logo')->move(storage_path('app\logo\\'.$section->id), $imageName);
        //получение id выбранных пользователей и запись
        $usersId =  array_keys($request->post('users'));
        $section->users()->attach($usersId);

        return redirect('/sections')->with('success', 'Section has been added');
    }
    /**
    * Show a newly created resource in storage.
    *
    * Show a resource.
    * @param \App\Models\Section $section
    * @return \Illuminate\Http\Response
     */
    public function show(Section $section)
    {
        return view('sections.show', compact('section'));
    }
    /**
     * Edit the form for editing the specified resource.
     *
     * страница редактирования(В ЮИ я привык делать рендерм формы обновления  и функционал в одном методе)
     * @param \App\Models\Section $section
     * @return \Illuminate\Http\Response
     */
    public function edit(Section $section)
    {
        $users_id = $section->users()->pluck('user_id')->toArray();
        $users = User::select('id','name', 'email')->whereNotIn('id', $users_id)->get()->toArray();
        return view('sections.edit', compact('section','users'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Section  $section
     * Обновляем данные секции
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Section $section)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'description' => ['required', 'string'],
            'users' => 'required'
        ]);
        $imageName = $request->file('logo')->getClientOriginalName();
        $section->name = $request->get('name');
        $section->description = $request->get('description');
        $section->logo = $imageName;
        if($request->file('logo')){
            $request->validate(['logo' => 'required|image|mimes:jpeg,png,jpg,svg|max:2048']);
            if($request->file('logo')->move(storage_path('app\logo\\'.$section->id), $imageName)){
                $deletePath = storage_path('app\logo\\'.$section->id.'\\'.$section->logo);
                File::delete($deletePath);
                $section->logo = $imageName;
            }
        }
        //Вытаскиваем идентификаторы, затем синхронизируем с записью
        $usersId =  array_keys($request->post('users'));
        $section->users()->sync($usersId);
        if($section->save())
            return redirect('/sections')->with('success', 'Section has been updated');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  Section $section
     * @return \Illuminate\Http\Response
     */
    public function destroy(Section $section)
    {
        $section->delete();
        return redirect('/sections')->with('success', 'Section has been deleted Successfully');
    }
}
