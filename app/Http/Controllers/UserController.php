<?php

namespace App\Http\Controllers;

use \App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     * Вывод всех пользователей постранично
     */
    public function index()
    {
        $users = User::paginate(10);
        return view('users.index', compact('users'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * Сстраница создания пользователя
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('users.create');
    }
    /**
     * Store a newly created resource in storage.
     *
     * Создание пользователя
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);

        User::create([
            'name' => $request->get('name'),
            'email'=> $request->get('email'),
            'password'=> bcrypt($request->get('password'))
        ]);
        return redirect('/users')->with('success', 'User has been added');
    }
    /**
     * Display the specified resource.
     *
     * @param   User $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }
    /**
     * Show the form for editing the specified resource.
     *
     * Страница редактирования пользователя
     * @param   User $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        return view('users.edit', compact('user'));
    }
    /**
     * Update the specified resource in storage.
     *
     * Обновление данных пользователя
     * @param  Request  $request
     * @param   User $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => 'required|email|unique:users,email,'.$user->id,
            'password' => $request->get('password')? ['string', 'min:8'] : '',
        ]);

        $user->name = $request->get('name');
        $user->email  = $request->get('email');
        if($request->get('password')){
            $user->password = bcrypt($request->get('password'));
            #ЗДесь обычно идет спор ставить ли кавычки или нет, обычноя не ставлю когда строка одна, но по правилам вроде требуется
        }
        $user->save();
        return redirect('/users')->with('success', 'User has been updated');
    }

    /**
     * Remove the specified resource from database.
     *
     * УДаление пользователя
     * @param   User $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->delete();
        return redirect('/users')->with('success', 'User has been deleted Successfully');
    }
}
