<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User;
        $user->name = 'Jovert Palonpon';
        $user->email = 'admin@test.loc';
        $user->password = bcrypt('password');
        $user->email_verified_at = now();
        $user->remember_token = Str::random(10);
        $user->save();
        factory(User::class, 15)->create();
    }
}
