<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Models\Section;
use App\User;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/


$factory->define(Section::class, function (Faker $faker) {
    return [
        'name' => $faker->title,
        'description' => $faker->text,
        'logo' => $faker->image(storage_path('app\logo'), 640,480, null, false),
    ];


});
$factory->afterCreating(Section::class, function ($row, $faker) {
    $users = User::select('id')->inRandomOrder()
                ->limit(3)
                ->get()->toArray();
    $usersId = [];
    foreach ($users as $user){
        $usersId[] = $user['id'];
    }
    $row->users()->attach($usersId);
});
